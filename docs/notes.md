* Git-based sync system?
* Central management hub? We probably need one for load balancing / routing anyway.
* Advantages / disadvantages abstracted data synchronization
* https://www.tutorialspoint.com/distributed_dbms/distributed_dbms_databases.htm
* Strategies:
    - Replication
    - Fragmentation
* Fragmentation is already done on compoment level: components only communicate relevant data through REST.
* Stock is the most demanding when it comes to synchronization. Ordering could technically use the same synch strategy without functional drawbacks.
    * Stock could possibly fragment its data
* Can mention that distributed database systems are used to allow simple deployment, and microservice deployment of servers.
* Simpler strategies and synchronization algorithms have preference.
* Transaction-based 
    - easy diffs in latest-agent syncs
    - http://www.liquibase.org/
    - https://klonio.com/
    - https://www.plasticscm.com/home.html
    - https://www.symmetricds.org/

* Prototypes:
    - full data replication, synchronization after each query, central management hub
    - Optimized data replication
    - Delayed synchronization of non-critical data
    - p2p synchronization
    - literal git implementation

http://ryankirkman.com/2013/02/03/simple-relational-database-sync.html
https://www.nginx.com/blog/event-driven-data-management-microservices/

* Choose between reliability and availability?
    * Reliability can be asserted through a message broker