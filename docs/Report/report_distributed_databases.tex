\documentclass[]{article}
\usepackage{graphicx}

%opening
\title{Distributed Databases Report}
\author{Bob Steers}

\begin{document}

\maketitle

\begin{abstract}
	
Requirements, feasibility, and possible implementations of distributed databases for the Project Pub applications. A suggestion is made based on a combination of existing requirements, and industry best practices.

\end{abstract}

\section{Introduction}

In the context of Semester 6 (Enterprise software development), Project Pub is being developed. Project Pub is an application stack for day-to-day operations of a restaurant or pub. Ordering, stock, and drive-in modules are currently planned.

The implementation is a combination of loosely coupled Java EE components, with an eye on eventual deployment as micro-service applications.

This study concerns data management in a scenario where multiple Project Pub application servers are deployed to handle a single restaurant.

\section{Requirements in scope}

Any database management solution is primarily intended for use by the Project Pub application. The following requirements are defined there:

\begin{itemize}
	\item Each component has its own, separate database structure. There is no database-level communication or synchronization between them.
	\item Each component by default only uses one database structure, but any solutions should avoid dependencies on specific database engines or structures.
	\item Databases used by different Project Pub users (ie. different restaurants) should not communicate in any way.
	\item There are varying levels of required synchronicity. 
		\subitem Orders can be expected to be handled by one server only. Server B does not need to be aware of the order that was just handled by server A.
		\subitem Stock must be fully synchronized - any call to request current stock levels should return correct information.
\end{itemize}

In order to successfully implement Project Pub in a microservice architecture, the following requirements should be met:

\begin{itemize}
	\item The database structure should be fully distributed: there is no central source of truth.
	\item The system should be able to correctly dump all database data to a backup system.
\end{itemize}

As this project is done in the context of Semester 6 of Fontys ICT, there are some educational requirements:

\begin{itemize}
	\item Any prototypes are based on technologies featured in S6:
		\subitem Java EE
		\subitem Persistence frameworks
		\subitem REST 
\end{itemize}

\section{Requirements not in scope}

Some functionality is explicitly declared as out of scope. This could possibly be added at a future date, but for now its implementation (or lack of) is not a requirement.

\begin{itemize}
	\item Servers do not need to be able to hook up to an active environment: all servers are simultaneously brought online from a fixed state (same data).
	\item There are no numerical requirements to data safety. The less data lost when a server dies, the better, but guarantees are not required.
\end{itemize}

\section{Data distribution strategies}

In any scenario where a set of data is maintained by multiple "databases" of any kind, two approaches can be taken: \newline

\textbf{Duplication} 

Data is duplicated over all databases. The variables are how much data is duplicated, and how soon. This strategy increases reliability and redundancy. If each database contains all data, then no data is lost if one is destroyed.

However, this approach is less scalable. Total data size will be data size * number of databases, and each database will need to synchronize data with all other databases. \newline

\textbf{Fragmentation}

All databases only contain a subset of the total dataset. This approach allows a combination of database types, to optimise performance and availability of a wide range of data types.

Its scalability comes at a cost: there are more moving parts to the complete "database", and any lookup becomes a two-step process. The relevant database has to be found before performing the actual query.\linebreak

Requirements decide this issue for us: it is explicitly stated that every new host should replicate the full software stack. Data fragmentation would run counter to this approach of using duplicated servers.

\section{ACIDity}

The ACID acronym describes the guarantees made by a database system.

\begin{itemize}
	\item \textbf{Atomicity} Changes are made atomically
	\item \textbf{Consistency} The database state and behaviour is always consistent
	\item \textbf{Isolation} Concurrent transactions do not influence each other
	\item \textbf{Durability} Once a transaction has been committed it is not undone
\end{itemize}

Normally, this is guaranteed by the database engine. In a distributed system where individual databases are not directly aware of their peers, no such guarantee is implicit.

\section{Locking implementation}

It is certainly possible to implement a strategy where multiple hosts can commit transactions while guaranteeing ACIDity.

\begin{figure}[]
	\centering
	\includegraphics[width=\linewidth]{"Simple locked synchronization scheme".png}
	\caption{Simple synchronization}
	\label{fig:simplelocking}
\end{figure}

Figure \ref{fig:simplelocking} shows a possible implementation. It requires multiple agents running the full application stack (Agent1, Agent2), and an administration server (Admin).

For every transaction, the current agent acquires the lock, and the address of the last agent to make a transaction. This last agent is considered to hold the current truth of the database. 

After the transaction, the lock is released.

This approach guarantees that transactions are executed in order, and against the latest state of the database. The problem, however, is that it scales very badly: only one agent can commit simultaneously.

It offers reasonable redundancy: in the worst case scenario (a host crashes just after committing), only the most recent commits are lost. 
Performance-wise, it is strictly worse than a single database host. Multiple HTTP round-trips are required in order to lock/unlock, and synchronization is done while users are waiting for a transaction to complete.

\section{Event-driven synchronization}

An alternative approach relaxes the requirement of ordered inserts, in favour of scalability and performance.

\begin{figure}[]
	\centering
	\includegraphics[width=\linewidth]{"Event-based database synchronization-1".png}
	\caption{Published transactions}
	\label{fig:broadcasting}
\end{figure}

Figure \ref{fig:broadcasting} shows the basic implementation. Multiple agents make independent commits to the database state, and publish their transaction to a message broker.

The broker broadcasts the transaction to all other agents, who mirror the change to their database.

The advantages compared to simple locked synchronization is that the databases are synchronized piecemeal when not actively handling user queries, and that multiple agents can make concurrent transactions.

\begin{figure}[]
	\centering
	\includegraphics[width=\linewidth]{"Event-based database synchronization-2".png}
	\caption{Publishing race condition}
	\label{fig:broadcasting_race}
\end{figure}

\section{Event-driven race conditions}

Figure \ref{fig:broadcasting_race} demonstrates a significant drawback to publishing approach: the transaction order in synchronized databases does not match.

The second (concurrent) action flow is in blue. After both action(1) and action(2) are completed, the databases performed the transactions in differing orders.

Agent 1 and 3 committed transaction 1 before transaction 2, but Agent 2 performed action 2 (transaction 2) before being notified of transaction 1.

This could be solved by requiring published transactions to always describe the change to the database, and not the new state.

Correct insertion can also be handled by providing every transaction with two identifiers: a time stamp, and a 128-bit GUID (unique for all practical purposes).

All transactions are initially sorted by insertion time. Exactly concurrent insertions are resolved by sorting them on GUID. This guarantees that while the insertion order of transactions may not be perfect, the resulting database state is equal on all hosts.

\section{Requirements: reality check}

The requirements state that any database transactions within the distributed system should be atomic, and in correct order. Both proposed implementations fulfil the atomicity requirement, but only the locking implementation guarantees correct ordering.

However, it does so only by taking a large hit to performance.

A question might be: does the case (the stock system for a restaurant) really require this guarantee? No stock management system will ever reach 100\% correctness at all times.

Theft, waste, and human error by suppliers all but guarantee that physical stock count, and the number tracked by software will slowly diverge. Most companies use regular stock counts to recalibrate their software.

\section{Undoing transactions}

In a perfect world, an event-driven system continuously merging transactions always is in happy flow. As it is not a perfect world, it needs to be capable of recovering from data corruption or faulty transactions.

Faulty transactions can be handled through the publishing mechanism: broadcast an event that a specific transaction should be rolled back. Its GUID can be used as an easy identifier.

\section{Fixed points}

Continuously constructing the current state of the database by stacking transactions will slow down with the number of relevant entries.

For performance and stability purposes, the system can use fixed recovery states. This can take the form of explicitly setting absolute database state, and only calculating transactions made after this point.
This mechanism improves performance (only the most recent transactions need to be taken together), and offers a manual error recovery mechanism for catastrophic data corruption.

In this scenario, a system administrator would manually correct one of the databases, choose an insertion time, and propagate this new fixed starting point to all peer systems.

\section{Conclusion}

Within the scope of the requirements, the data storage strategy should be to duplicate data on multiple servers, and have them continuously synchronize.
When it comes to the manner of synchronization, there is a contradiction in requirements. One one hand it requires ordered correctness, and on the other it wants scalability by deploying multiple servers with synchronized databases.

A critical look at the requirements calls the need for perfectly accurate insertion order in question. 

As long as transactions are correctly phrased ('stock count++', and not 'stock count = value'), the transaction publishing approach seems to make acceptable sacrifices for a significant gain in scalability and performance.

\section{Further work}

The next step should be to produce simple prototypes for various alternatives, and measure their performance and accuracy under significant loads.

\end{document}
