# Distributed Database Management Project Pub

## Requirements in scope

Any database management solution is primarily intended for use by the Project Pub application. The following requirements are defined there:

* Each component has its own, separate database structure. There is no database-level communication or synchronization between them.
* Each component by default only uses one database structure. There is no requirement for compatibility with multiple database providers. (should be avoided)
* Databases used by different Project Pub users (ie. different restaurants) should not communicate in any way.
* There are varying levels of required synchronicity:
    * Orders can be expected to be handled by one server only. Server B does not need to be aware of the order that was just handled by server A.
    * Stock must be fully synchronized - any call to request current stock levels should return correct information.

In order to successfully implement Project Pub in a microservice architecture, the following requirements should be met:

* The database structure should be fully distributed: there is no central source of truth.
* The system should be able to correctly dump all database data to a backup system.

As this project is done in the context of Semester 6 of Fontys ICT, there are some educational requirements:

* Any prototypes are based on technologies featured in S6:
    - Java EE
    - Persistence frameworks
    - REST

## Requirements not in scope

Some functionality is explicitly declared as out of scope. This could possibly be added at a future date, but for now its implementation (or lack of) is not a requirement.

* Servers do not need to be able to hook up to an active environment: all servers are simultaneously brought online from a fixed state (same data).
* There are no numerical requirements to data safety. The less data lost when a server dies, the better, but guarantees are not required.

## Implementation

This project has the following deliverables:

* A research document detailing:
    * Known best practices.
    * Standard designs an implementations.
    * Technical feasibility of requirements.
    * Evaluation of available technologies and designs.
    * Estimated implementation effort for any feasible approaches.

* Protoypes for one or more of the most promising implementations. These should demonstrate feasibility and generic approach. They do not need to replicate all of Project Pub's design and functionality.

